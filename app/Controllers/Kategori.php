<?php namespace App\Controllers;


use App\Models\model_kategori;

class Kategori extends BaseController
{
	protected $model_kategori;

	public function __construct(){
		$this->model_kategori = New model_kategori();
	}

	public function index()
	{
		$validation = \Config\Services::validation();
		$kategori = $this->model_kategori->findAll();
		$data= [
			'title' => 'Data Kategori',
			'validation' => $validation,
			'kategori' => $kategori
			];
		//print_r($kategori);
		return view('kategori/index',$data);
		
		
		
	}

	//simpan
	public function input()
	{
		$validation = \Config\Services::validation();

		if (isset($_POST['save'])) {	
			//validasi input
			if(!$this->validate([
				'category_name' => 'required|is_unique[category.category_name]'
			])){
				
				session()->setFlashdata('error','Nama Kategori Harus Unik');
				return redirect()->to('/Kategori')->withInput();
			}

			
			$this->model_kategori->save([
				'category_name' =>$this->request->getVar('category_name')
			]);
			session()->setFlashdata('pesan','Data berhasil ditambah.');
			return redirect()->to('/Kategori');
		}else{
			
			$data= [
				'title' => 'Input Data Kategori',
				'validation' => $validation
				];
			return view('kategori',$data);

		}
	}

	//delete
	public function delete($id)
	{
		//cari name file berdasarkan id
		
		$this->model_kategori->delete($id);
		$this->model_kategori->delete_produk_kategori($id);
		session()->setFlashdata('pesan','Data berhasil dihapus');
		return redirect()->to('/Kategori');
	}
	//--------------------------------------------------------------------

	//edit
	public function edit($id)
	{
		$validation = \Config\Services::validation();
		$kategori = $this->model_kategori->find($id);
		$data= [
			'title' => 'Input Data Produk',
			'kategori' =>$kategori,
			'validation' => $validation
		];
		//print_r($produk[0]->product_id);
		return view('Kategori/edit',$data);
	}


	//update
	public function update()
	{
		$category_id = $this->request->getVar('category_id');
		
		if(!$this->validate([
			'category_name' => "required|is_unique[category.category_name,category_id,{$category_id}]"
	
		])){
			
			session()->setFlashdata('error','Nama Kategori Harus Unik');
			return redirect()->to('/Kategori')->withInput();
		}

		//save data kl
		$this->model_kategori->save([
			'category_id' => $category_id,
			'category_name' =>$this->request->getVar('category_name')
		]);
		session()->setFlashdata('pesan','Data berhasil diubah');
		return redirect()->to('/Kategori');
	}
}
