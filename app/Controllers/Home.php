<?php namespace App\Controllers;


use App\Models\model_produk;

class Home extends BaseController
{
	protected $model_produk;

	public function __construct(){
		$this->model_produk = New model_produk();
	}

	public function index()
	{
		
		$produk = $this->model_produk->findAll();
		$data= [
			'title' => 'Data Produk',
			'bidang' => $produk
			];
		//print_r($produk);
		return view('produk/index',$data);
		
		
		
	}

}
