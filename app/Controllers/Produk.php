<?php namespace App\Controllers;


use App\Models\model_produk;
use App\Models\model_kategori;
use TCPDF;

class Produk extends BaseController
{
	protected $model_produk;
	protected $model_kategori;
	public function __construct(){
		$this->model_produk = New model_produk();
		$this->model_kategori = new model_kategori();
	}

	public function index()
	{
		
		$validation = \Config\Services::validation();
		$kategori = $this->model_kategori->findAll();
		$produk = $this->model_produk->get_all_produk_kategori();
		$data= [
			'title' => 'Data Produk',
			'validation' => $validation,
			'kategori' =>$kategori,
			'produk' => $produk
			];
		return view('produk/index',$data);
		
		
		
	}


	//simpan
	public function input()
	{
		$validation = \Config\Services::validation();
		
		if (isset($_POST['save'])) {	
			//validasi input
			if(!$this->validate([
				'product_name' => 'required|is_unique[product.product_name]',
				'product_price' => 'required',
				'product_category_id' => 'required'
			])){
				
				
				return redirect()->to('/Produk/input')->withInput();
			}

			
			//save data produk
			$this->model_produk->save([
				'product_name' =>$this->request->getVar('product_name'),
				'product_price' =>$this->request->getVar('product_price'),
				'product_category_id' =>$this->request->getVar('product_category_id')
			]);
			session()->setFlashdata('pesan','Data berhasil ditambah.');
			return redirect()->to('/Produk');
		}else{
			$kategori = $this->model_kategori->findAll();
			$data= [
				'title' => 'Input Data Produk',
				'kategori' => $kategori,
				'validation' => $validation
				];
			return view('produk/input',$data);
		}
	}

	//delete
	public function delete($id)
	{
		//cari name file berdasarkan id
		
		$this->model_produk->delete($id);
		session()->setFlashdata('pesan','Data berhasil dihapus');
		return redirect()->to('/Produk');
	}
	//--------------------------------------------------------------------


	//edit
	public function edit($id)
	{
		$validation = \Config\Services::validation();
		$produk = $this->model_produk->get_by_id($id);
		$kategori = $this->model_kategori->findAll();
		$data= [
			'title' => 'Input Data Produk',
			'produk' => $produk,
			'kategori' =>$kategori,
			'validation' => $validation
		];
		//print_r($produk[0]->product_id);
		return view('Produk/edit',$data);
	}


	//update
	public function update($id)
	{
		$product_id = $this->request->getVar('product_id');
		
		if(!$this->validate([
			'product_name' => "required|is_unique[product.product_name,product_id,{$product_id}]",
			'product_price' => 'required',
			'product_category_id' => 'required'
	
		])){
			
			return redirect()->to('/Produk/edit/'.$product_id)->withInput();
		}

		//save data kl
		$this->model_produk->save([
			'product_id' => $product_id,
			'product_name' =>$this->request->getVar('product_name'),
			'product_price' =>$this->request->getVar('product_price'),
			'product_category_id' =>$this->request->getVar('product_category_id')
		]);
		session()->setFlashdata('pesan','Data berhasil diubah');
		return redirect()->to('/Produk');
	}

	public function cetak()
	{

		if (!empty($this->request->uri->getSegment(3))) {
			$id_kategori = $this->request->uri->getSegment(3);
			$kategori = $this->model_kategori->find($id_kategori);
			$produk = $this->model_produk->get_produk_bycat($id_kategori);
			$jenis = $kategori['category_name'];
			
			
			//dd($produk);
		}else{
			$produk = $this->model_produk->get_all_produk_kategori();
			$jenis ="Semua kategori";
		}
		$data= [
				'title' => 'Data Produk',
				'jenis' => $jenis,
				'produk' => $produk
				];
			//print_r($produk);
		// return view('produk/print',$data);

		$html = view('produk/print',$data);

		$pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Abdullah Aflaha Aslam');
		$pdf->SetTitle('Data Produk');
		$pdf->SetSubject('List Produk');
		$pdf->SetMargins('30', '4', '30');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->addPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		$this->response->setContentType('application/pdf');
		$pdf->Output('Data Produk.pdf', 'I');
     }
}
