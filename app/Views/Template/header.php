<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Admin Tang_Go</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/app.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/jqvmap/dist/jqvmap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/weather-icon/css/weather-icons.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/weather-icon/css/weather-icons-wind.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/summernote/summernote-bs4.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('assets/bundles/bootstrap-daterangepicker/daterangepicker.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('assets/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/select2/dist/css/select2.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/jquery-selectric/selectric.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('assets/bundles/bootstrap-timepicker/css/bootstrap-timepicker.min.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/summernote/summernote-bs4.css'); ?>">
    <!-- Data table -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bundles/datatables/datatables.min.css'); ?>">
    <link rel="stylesheet"
        href="<?php echo base_url('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css'); ?>">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/components.css'); ?>">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url('assets/img/logo-bappenas.png'); ?>' />

    <!-- Load librari/plugin jquery nya -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>" type="text/javascript"></script>

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#test').attr('src', e.target.result);
                //$("#test").css("display", "block");
                /* $('#test').css({"display":"block","float":"left" });*/

            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>

    <script>
       $(document).ready(function(){
            $(function () {
                $('#ModalEditKategori').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); /*Button that triggered the modal*/
                    var id = button.data('id');
                    var name = button.data('name');
                    var modal = $(this);
                    modal.find('#category_id').val(id);
                    modal.find('#category_name').val(name);
                });
                $('#ModalEditProduk').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); /*Button that triggered the modal*/
                    var id = button.data('id');
                    var name = button.data('name');
                    var modal = $(this);
                    modal.find('#product_id').val(id);
                    modal.find('#category_name').val(name);
                });
            });
        });
    </script>
</head>
