 <!--MOdal INput Data-->
 <div class="modal fade" id="ModalInputKategori" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModal">Input Kategori
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('Kategori/input/'); ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field(); ?>
          <div class="section-title mt-0">Nama Kategori</div>
            <div class="form-group">
              <input type="text" class="form-control  " name="category_name" value="<?= old('category_name') ?>" require>
              <div class="invalid-feedback">
               
                  
                </div>
              </div>
            <div class="form-group mb-0">
          </div>
          <button type="Submit" name="save" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
          </form>
        </div>
    </div>
  </div>
</div>
 <!--MOdal Edit Kategori--> <
 <div class="modal fade" id="ModalEditKategori" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModal">Edit Kategori
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('Kategori/update/'); ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field(); ?>
        <input type="hidden" class="form-control" id="category_id" name="category_id" value="<?= old('category_id') ?>" require>
          <div class="section-title mt-0">Nama Kategori</div>
            <div class="form-group">
              <input type="text" class="form-control" id="category_name" name="category_name" value="<?= old('category_name') ?>" require>
              <div class="invalid-feedback">
               
                  
                </div>
              </div>
            <div class="form-group mb-0">
          </div>
          <button type="Submit" name="save" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
          </form>
        </div>
    </div>
  </div>
</div>


 <!--MOdal Edit Produk--> <
 <div class="modal fade" id="ModalEditProduk" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModal">Input Kategori
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('Kategori/input/'); ?>" method="post" enctype="multipart/form-data">
        <?= csrf_field(); ?>
          <div class="section-title mt-0">Nama Kategori</div>
            <div class="form-group">
              <input type="hidden" class="form-control" id="category_id" name="category_id" value="<?= old('category_name') ?>" require>
              <input type="text" class="form-control" id="category_name" name="category_name" value="<?= old('category_name') ?>" require>
              <div class="invalid-feedback">
               
                  
                </div>
              </div>
            <div class="form-group mb-0">
          </div>
          <button type="Submit" name="save" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
          </form>
        </div>
    </div>
  </div>
</div>


        <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; Teng_GO 2020 <div class="bullet"></div></a>
        </div>
        <div class="footer-right">
        </div>
      </footer>
    </div>
  </div>

  
   <!-- General JS Scripts -->
  <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
  <!-- JS Libraies -->
  <script src="<?php echo base_url('assets/bundles/datatables/datatables.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jquery-ui/jquery-ui.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/chartjs/chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/apexcharts/apexcharts.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jquery.sparkline.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jqvmap/dist/jquery.vmap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jqvmap/dist/maps/jquery.vmap.indonesia.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/cleave-js/dist/cleave.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/cleave-js/dist/addons/cleave-phone.us.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jquery-pwstrength/jquery.pwstrength.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/bootstrap-timepicker/js/bootstrap-timepicker.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/select2/dist/js/select2.full.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/jquery-selectric/jquery.selectric.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/summernote/summernote-bs4.js'); ?>"></script>
  <script src="<?php echo base_url('assets/bundles/sweetalert/sweetalert.min.js'); ?>"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo base_url('assets/js/page/datatables.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/page/index2.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/page/todo.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/page/forms-advanced-forms.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/page/sweetalert.js'); ?>"></script>
  <!-- Template JS File -->
  <script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
</body>

</html>