<?= $this->extend('Template/template')?>

<?= $this->section('contents') ?>

<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="card-body">
                <?php
                    if(session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success alert-dismissible show fade col-6">
                    <div class="alert-body ">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        <?= session()->getFlashdata('pesan'); ?>.
                    </div>
                </div>
                <?php endif; ?>
                <!-- <a href="<?php echo base_url('Produk/input'); ?>" class="btn btn-icon btn-primary"><i class="far fa-edit"></i> Input</a>
                <a href="<?php echo base_url('Produk/cetak'); ?>" 
                        class="btn btn-warning pull-right btn-icon" target="blank"><i class="fas fa-print"></i> Print</a> -->
                <div class="dropdown ">
                   <a href="<?php echo base_url('Produk/input'); ?>" class="btn btn-icon btn-primary"><i class="far fa-edit"></i> Input</a>
                    <a href="#" data-toggle="dropdown"
                        class="btn btn-warning dropdown-toggle pull-right"><i class="fas fa-print"></i> Print</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item has-icon" target="blank" href="<?php echo base_url('Produk/cetak'); ?>"></i> Semua Kategori</a>
                        <?php 
                        foreach ($kategori as $kategori) {
                        ?>
                        <a class="dropdown-item has-icon" target="blank"  href="<?php echo base_url('Produk/cetak/').'/'.$kategori['category_id'];?>"><i
                                class="fas upload-cloud"></i> <?= $kategori['category_name'] ?></a>
                        <?php }?>
                        
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">
                                    #
                                </th>
                                <th>Nama Produk</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                                <th class="text-center" width="25%">action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                              $i = 1;
                              foreach ($produk as $produk) {
                              ?>
                            <tr>
                                <td class="text-center"><?php echo $i; $i++ ?></td>
                                <td><?= $produk->product_name ?></td>
                                <td><?= $produk->category_name ?></td>
                                <td><?= $produk->product_price ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('Produk/edit/'.$produk->product_id); ?>"
                                        class="btn btn-icon btn-sm btn-primary" title="Edit"><i
                                            class="fas fa-edit"></i></a> </i></a> |
                                    <form action="/Produk/<?= $produk->product_id ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-icon btn-sm btn-primary"
                                            onclick="return confirm('Delete Selected Data?'); ">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>

                                </td>

                            </tr>
                            <?php
                              }
                              ?>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>