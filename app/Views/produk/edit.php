<?= $this->extend('Template/template')?>


<?= $this->section('contents') ?>


<div class="row">
    <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <form action="<?php echo base_url('Produk/update/'.$produk[0]->product_id); ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <div class="card-body">
                    <input type="hidden" name="product_id" value="<?= $produk[0]->product_id; ?>">
                   

                  <!--   <div class="section-title mt-0">Harga</div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    IDR
                                </div>
                            </div>
                             <input type="number" class="form-control currency <?= ($validation->hasError('product_price')) ?'is-invalid' : ''; ?>" name="product_price" value="<?= (old('product_price')) ? old('product_price'):$produk[0]->product_price ?>" required>

                        </div>
                    </div>
 -->
                    <div class="section-title mt-0">Nama Produk</div>
                    <div class="form-group">
                        <!-- <label></label> -->
                        <input type="text" class="form-control currency <?= ($validation->hasError('product_name')) ?'is-invalid' : ''; ?>" name="product_name" value="<?= (old('product_name')) ? old('product_name'):$produk[0]->product_name ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError('product_name') ?>
                        </div>
                    </div>

                                        <div class="section-title mt-0">Kategori</div>
                    <div class="form-group">
                        <!-- <label></label> -->
                        <select class="form-control selectric" name="product_category_id" required>
                            <option value="">Pilih Kategori </option>
                            <?php 
                        foreach ($kategori as $kategori) {
                             ?>
                            <option value="<?= $kategori['category_id'] ?>"
                                <?php if(empty(old('product_category_id')) && $kategori['category_id'] == $produk[0]->product_category_id) { echo "selected";
                                }elseif (old('product_category_id')== $kategori['category_id']) {
                                   echo "selected";
                                }; ?> >
                                <?= $kategori['category_name'] ?>
                            </option>
                            <?php }
                                ?>
                        </select>
                    </div>

                    <div class="section-title mt-0">Harga</div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    IDR
                                </div>
                            </div>
                             <input type="number" class="form-control currency <?= ($validation->hasError('product_price')) ?'is-invalid' : ''; ?>" name="product_price" value="<?= (old('product_price')) ? old('product_price'):$produk[0]->product_price ?>" required>

                        </div>
                    </div>

                </div>
                <div class="card-footer text-right">
                    <input type="submit" class="btn btn-primary" value="Save" name="update">
                </div>
            </form>
        </div>
    </div>

</div>


<?= $this->endSection(); ?>