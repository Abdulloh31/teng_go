<?= $this->extend('Template/template')?>

<?= $this->section('contents') ?>


<div class="row">
    <div class="col-6 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><?php echo $title; ?></h4>
            </div>

            <form action="<?php echo base_url('Produk/input/'); ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <div class="card-body">
                    
                    <div class="section-title mt-0">Nama Produk</div>
                    <div class="form-group">
                        <!-- <label></label> -->
                        <input type="text"
                            class="form-control  <?= ($validation->hasError('product_name')) ?'is-invalid' : ''; ?>" name="product_name"
                            value="<?= old('product_name') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('product_name') ?>
                        </div>
                    </div>

                    <div class="section-title mt-0">Kategori</div>
                    <div class="form-group">
                        <!-- <label></label> -->
                        <select class="form-control selectric" name="product_category_id" required>
                            <option value="">Pilih Kategori </option>
                            <?php 
                        foreach ($kategori as $kategori) {
                             ?>
                            <option value="<?= $kategori['category_id'] ?>"
                                <?= (old('product_category_id')== $kategori['category_id']) ?'selected' : ''; ?>><?= $kategori['category_name'] ?>
                            </option>
                            <?php }
                                ?>
                        </select>
                    </div>

                   <div class="section-title mt-0">Harga</div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    IDR
                                </div>
                            </div>
                            <input type="number" class="form-control currency" name="product_price"
                                value="<?= old('product_price') ?>" required>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <input type="submit" class="btn btn-primary" value="Save" name="save">
                </div>
            </form>
        </div>
    </div>

</div>


<?= $this->endSection(); ?>