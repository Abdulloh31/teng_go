<!DOCTYPE html>
<html lang="en">

<head>
<style>
    table {
        width: 100%;
        align-content: center;
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        margin-left: auto;
        margin-right: auto;
    }

    td { 
        text-align: center;
        vertical-align: center;
        height: 20px;
        margin: 8px;
        border: 1px solid #ddd;
        padding: 8px;
    }
    th{
        font-weight: bold;
        text-align: center;
        vertical-align: center;
        height: 20px;
        margin: 8px;
        border: 1px solid #ddd;
        padding: 8px;
    }
    table.center {
      margin-left: auto;
      margin-right: auto;
    }
    h1{
        font-size: 40px;
    }

</style>
</head>
    <div>
        <h1>Data Produk</h1>
        <h2>Kategori : <?= $jenis ?></h2>
        <p>Jakarta, <?php echo  date("d M Y");  ?></p>
        <hr>
    </div>

    <br>
    <div>

    <table  cellpadding="5" >
            <tr>
                <th width="5%">
                    #
                </th>
                <th >Nama Produk</th>
                <th >Kategori</th>
                <th>Harga</th>
            </tr>
    <?php if (empty($produk)): ?>
        <tr>
            <td colspan="4">Data Tidak Ditemukan</td>
        </tr>
    <?php else: ?>
            <?php 
    $i = 1;
    foreach ($produk as $produk) {
    ?>
            <tr>
                <td width="5%"><?php echo $i; $i++ ?></td>
                <td><?= $produk->product_name ?></td>
                <td><?= $produk->category_name ?></td>
                <td>Rp <?= number_format($produk->product_price,2,',','.') ?></td>

            </tr>
            <?php
    }
    ?>
    <?php endif ?>
    </table>
    </div>
</body>

</html>