<?= $this->extend('Template/template')?>

<?= $this->section('contents') ?>

<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                <h4><?php echo $title; ?></h4>
            </div>
            <div class="card-body">

                <?php
                    if(session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success alert-dismissible show fade col-6">
                    <div class="alert-body ">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        <?= session()->getFlashdata('pesan'); ?>.
                    </div>
                </div>
                <?php endif; ?>
                 <?php
                    if(session()->getFlashdata('error')) : ?>
                <div class="alert alert-danger alert-dismissible show fade col-6">
                    <div class="alert-body ">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        <?= session()->getFlashdata('error'); ?>.
                    </div>
                </div>
                <?php endif; ?>
                <p>
                    <a class="btn btn-icon btn-primary" data-toggle="modal" href="#ModalInputKategori"><i class="far fa-edit"></i> Input</a>
                    <!-- <a href="<?php echo base_url('Kategori/input'); ?>" class="btn btn-icon btn-primary"><i
                            class="far fa-edit"></i> Input</a> -->
                </p>
                <div class="table-responsive">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">
                                    #
                                </th>
                                <th>Nama Kategori</th>
                                <th class="text-center" width="25%">action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                              $i = 1;
                              foreach ($kategori as $kategori) {
                              ?>
                            <tr>
                                <td class="text-center"><?php echo $i; $i++ ?></td>
                                <td><?= $kategori['category_name'] ?></td>
                                <td class="text-center">
                                    <!-- <a href="<?php echo base_url('Kategori/edit/'.$kategori['category_id']); ?>"
                                        class="btn btn-icon btn-sm btn-primary" title="Edit"><i
                                            class="fas fa-edit"></i></a> </i></a> --> 
                                    <button type="button" class="btn btn-icon btn-sm btn-primary" data-toggle="modal" href="#ModalEditKategori" data-backdrop="static"  data-id="<?= $kategori['category_id']?>" data-name="<?= $kategori['category_name']?>"><i
                                            class="fas fa-edit"></i></button> |
                                    <form action="/Kategori/<?= $kategori['category_id'] ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-icon btn-sm btn-primary"
                                            onclick="return confirm('Delete Selected Data?'); ">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>

                                </td>

                            </tr>
                            <?php
                              }
                              ?>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->endSection(); ?>