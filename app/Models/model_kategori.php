<?php

namespace App\Models;

use CodeIgniter\Model;

class model_kategori extends Model {

    protected $table = 'category';
    protected $primaryKey = 'category_id';
    protected $useTimestamps = false;
    protected $allowedFields = ['category_name'];
    
    function get_kategori_id($id = false) {
        if ($id === false) {
            return $this->findAll();
        } else {
            return $this->getWhere(['category_id' => $id])->getRowArray();
        }
    }

    public function delete_produk_kategori($id)
    {
       return $this->db->query("DELETE FROM product WHERE product_category_id = $id;");
    }

}
