<?php

namespace App\Models;

use CodeIgniter\Model;

class model_produk extends Model {

    protected $table = 'product';
    protected $primaryKey = 'product_id';
    protected $useTimestamps = false;
    protected $allowedFields = ['product_name','product_price','product_category_id'];

     function get_all_produk_kategori() {
        return $this->db->query("select * from product LEFT JOIN category ON product.product_category_id = category.category_id ; ")->getResult();
    }

    function get_by_id($id) {
        return $this->db->query("select * from product  LEFT JOIN category ON product.product_category_id = category.category_id where product_id = $id; ")->getResult();
    }
    function get_produk_bycat($id) {
        return $this->db->query("select * from product  LEFT JOIN category ON product.product_category_id = category.category_id where product_category_id = $id; ")->getResult();
    }

}
